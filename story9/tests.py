from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from .views import *
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class Story9Test(TestCase):

    def test_story_9_url_is_exist(self):
        response = Client().get('/story9/')
        self.assertEqual(response.status_code, 200)

    def test_story_9_using_index_func(self):
        found = resolve('/story9/')
        self.assertEqual(found.func, index)

    def test_books_url_is_exist(self):
        response = Client().get('/story9/google-apis-json-books/?search=quilting')
        self.assertEqual(response.status_code, 200)

    def test_no_books_url_is_exist(self):
        response = Client().get('/story9/google-apis-json-books/')
        self.assertEqual(response.status_code, 200)
# class Story9FunctionalTest(LiveServerTestCase):
#
#     def setUp(self):
#         chrome_options = Options()
#         chrome_options.add_argument('--dns-prefetch-disable')
#         chrome_options.add_argument('--no-sandbox')
#         chrome_options.add_argument('--headless')
#         chrome_options.add_argument('disable-gpu')
#         self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
#         super(Story9FunctionalTest, self).setUp()
#
#     def tearDown(self):
#         self.selenium.quit()
#         super(Story9FunctionalTest, self).tearDown()
#
#     def test_favorite_button(self):
#         selenium = self.selenium
#         selenium.get(self.live_server_url + '/story9/')  # use the live server url
#         time.sleep(2)
#         search_bar = selenium.find_element_by_id("search")
#         search_bar.send_keys("Quilting")
#         time.sleep(4)
#         total_fav = selenium.find_element_by_class_name("favorites-count")
#         self.assertEqual(0, int(total_fav.text))
#         time.sleep(2)
#         fav_button1 = selenium.find_element_by_class_name("notfavorited")
#         time.sleep(1)
#         fav_button1.click()
#         time.sleep(1)
#         self.assertEqual(1, int(total_fav.text))
#         time.sleep(1)
#         fav_button1.click()
#         time.sleep(1)
#         self.assertEqual(0, int(total_fav.text))
#         time.sleep(1)
#
#
