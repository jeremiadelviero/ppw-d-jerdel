from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from .views import *
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
import time


# Create your tests here.
class Story8Test(TestCase):

    def test_landing_page_url_is_exist(self):
        response = Client().get('/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_index_func(self):
        found = resolve('/')
        self.assertEqual(found.func, index)

    def test_portfolio_url_is_exist(self):
        response = Client().get('/portfolio/')
        self.assertEqual(response.status_code, 200)

    def test_portfolio_using_portfolio_func(self):
        found = resolve('/portfolio/')
        self.assertEqual(found.func, portfolio)

    def test_about_url_is_exist(self):
        response = Client().get('/about/')
        self.assertEqual(response.status_code, 200)

    def test_about_using_about_func(self):
        found = resolve('/about/')
        self.assertEqual(found.func, about)

    def test_contact_url_is_exist(self):
        response = Client().get('/contact/')
        self.assertEqual(response.status_code, 200)

    def test_contact_using_contact_func(self):
        found = resolve('/contact/')
        self.assertEqual(found.func, contact)


class Story8FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story8FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story8FunctionalTest, self).tearDown()

    def test_input_todo(self):
        selenium = self.selenium
        selenium.get(self.live_server_url)  # use the live server url
        self.assertEqual(selenium.find_element_by_class_name("loader").value_of_css_property('display'),
                         'block')
        time.sleep(5)
        self.assertEqual(selenium.find_element_by_class_name("loader").value_of_css_property('display'),
                         'none')
        page = selenium.page_source
        self.assertEqual('JerDelviero | Home', selenium.title)
        self.assertIn('Currently majoring Computer Science in University of Indonesia.', page)

        selenium.find_element_by_id('accordion2').click()
        self.assertIn('Pursuing my Bachelor Degree', selenium.page_source)
        selenium.find_element_by_id('accordion3').click()
        self.assertIn('Vice Head of Rohani Kristen SMAN 1 Bekasi 2016 - 2017', selenium.page_source)
        selenium.find_element_by_id('accordion4').click()
        self.assertIn('Semifinalist at EURECA Business Plan Competition Prasetiya Mulya University',
                      selenium.page_source)
        # find the form element
        theme_button = selenium.find_element_by_id('button-theme')

        navbar = selenium.find_element_by_id('responsive-menu')
        self.assertEqual(navbar.value_of_css_property('background-color'), 'rgba(234, 202, 33, 1)')
        # submitting the form

        theme_button.click()
        time.sleep(1)
        navbar = selenium.find_element_by_id('responsive-menu')
        self.assertEqual(navbar.value_of_css_property('background-color'), 'rgba(6, 54, 132, 1)')
