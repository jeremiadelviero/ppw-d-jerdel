from django import forms


class SubscribeForm(forms.Form):
    error_messages = {
        'required': 'Tolong isi input ini',
        'invalid': 'Isi input dengan email',
    }
    attrs = {
        'onchange': 'check_all()',
        'onkeyup': 'check_all()',
    }
    attrs_email = {
        'onchange': 'check_all(); check_email()',
        'onkeyup': 'check_all(); check_email()',
    }
    name = forms.CharField(label='Username', required=True, max_length=300,
                           widget=forms.TextInput(attrs=attrs))
    email = forms.EmailField(label='Email', required=True, max_length=300,
                             widget=forms.EmailInput(attrs=attrs_email))
    password = forms.CharField(label='Password', required=True, max_length=300,
                               widget=forms.PasswordInput(attrs=attrs))


class EmailValidator(forms.Form):
    email = forms.EmailField(label='Email', required=True, max_length=300)
