from django.test import TestCase, LiveServerTestCase
from django.test import Client
from django.urls import resolve
from .views import *
from .models import *
from selenium import webdriver
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.keys import Keys
import time


class Story10Test(TestCase):

    def test_subscribe_page_url_is_exist(self):
        response = Client().get('/subscribe/')
        self.assertEqual(response.status_code, 200)

    def test_landing_page_using_index_func(self):
        found = resolve('/subscribe/')
        self.assertEqual(found.func, index)

    def test_model_can_create_new_subscriber(self):
        Subscriber.objects.create(name="Tester", email="test@email.com", password="password")

        self.assertEqual(Subscriber.objects.all().count(), 1)

    def test_model_subscriber_str(self):
        Subscriber.objects.create(name="Tester", email="test@email.com", password="password")
        self.assertEqual("test@email.com", str(Subscriber.objects.get(pk=1)))

    def test_can_save_a_POST_request(self):
        self.client.post('/subscribe/',
                         data={'name': "Tester", 'email': "test@email.com", 'password': "password"})
        counting_all_available_activity = Subscriber.objects.all().count()
        self.assertEqual(counting_all_available_activity, 1)

    def test_check_email_registered(self):
        Subscriber.objects.create(name="Tester", email="test@email.com", password="password")

        response = self.client.post('/subscribe/check_email',
                                    data={'email': "test@email.com"})
        response = response.json()
        self.assertTrue(response['email_registered'])

    def test_check_email_unregistered(self):
        response = self.client.post('/subscribe/check_email',
                                    data={'email': "test@email.com"})
        response = response.json()
        self.assertFalse(response['email_registered'])

    def test_check_all_form_complete_and_unregistered_email(self):
        response = self.client.post('/subscribe/check_all',
                                    data={'name': "Tester", 'email': "test@email.com", 'password': "password"})
        response = response.json()
        self.assertTrue(response['all_checked'])

    def test_check_all_form_incomplete(self):
        response = self.client.post('/subscribe/check_all',
                                    data={'name': "Tester", 'email': "falseEmail", 'password': "password"})

        response = response.json()
        self.assertFalse(response['all_checked'])


class Story10FunctionalTest(LiveServerTestCase):

    def setUp(self):
        chrome_options = Options()
        chrome_options.add_argument('--dns-prefetch-disable')
        chrome_options.add_argument('--no-sandbox')
        chrome_options.add_argument('--headless')
        chrome_options.add_argument('disable-gpu')
        self.selenium = webdriver.Chrome('./chromedriver', chrome_options=chrome_options)
        super(Story10FunctionalTest, self).setUp()

    def tearDown(self):
        self.selenium.quit()
        super(Story10FunctionalTest, self).tearDown()

    def test_input_form(self):
        selenium = self.selenium
        selenium.get(self.live_server_url + '/subscribe/')  # use the live server url

        name = selenium.find_element_by_id('id_name')
        email = selenium.find_element_by_id('id_email')
        password = selenium.find_element_by_id('id_password')
        submit = selenium.find_element_by_id('button-submit')

        name.send_keys("tester")
        email.send_keys("test@email.com")
        password.send_keys("password")
        time.sleep(2)
        submit.click()
