from django.shortcuts import render
from django.http import JsonResponse, HttpResponse
from .forms import SubscribeForm, EmailValidator
from .models import Subscriber
from django.core import serializers

active = 'active-page'


def index(request):
    response = {'subscribe': active, 'title': 'Subscribe', 'form': SubscribeForm}
    if request.method == 'POST':
        form = SubscribeForm(request.POST or None)
        if form.is_valid():
            subscriber = Subscriber(name=request.POST['name'], email=request.POST['email'],
                                    password=request.POST['password'])
            subscriber.save()
            data = {
                'post_success': True,
            }
            return JsonResponse(data)
    return render(request, 'subscribe/index.html', response)


def check_email_registered(request):
    if EmailValidator({'email': request.POST['email']}).is_valid():
        if Subscriber.objects.filter(email=request.POST['email']):
            return JsonResponse({'email_registered': True})
        return JsonResponse({'email_registered': False})


def check_all(request):
    if SubscribeForm(request.POST).is_valid() and not Subscriber.objects.filter(email=request.POST['email']):
        return JsonResponse({'all_checked': True})
    return JsonResponse({'all_checked': False})


def get_all(request):
    subscribers = Subscriber.objects.all()
    return HttpResponse(serializers.serialize('json', subscribers), content_type='application/json')


def unsubscribe(request):
    if request.method == 'POST':
        email = request.POST['email']
        password = request.POST['password']
        object = Subscriber.objects.filter(email=email, password=password)
        if object:
            object.delete()
            return JsonResponse({'delete_success': True})
        return JsonResponse({'delete_success': False})
