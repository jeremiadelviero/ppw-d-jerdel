from django.shortcuts import render
from django.http import JsonResponse
from django.views.decorators.csrf import csrf_exempt
import requests

active = 'active-page'


def index(request):
    response = {'story9': active, 'title': 'Story 9'}
    if request.user.is_authenticated:
        if "liked" not in request.session:
            request.session["fullname"] = request.user.first_name + " " + request.user.last_name
            request.session["username"] = request.user.username
            request.session["email"] = request.user.email
            request.session["sessionid"] = request.session.session_key
            request.session["liked"] = []
    if "liked" in request.session:
        response['totalfav'] = len(request.session['liked'])
    else:
        response['totalfav'] = 0
    return render(request, 'story9/index.html', response)


def google_apis_books(request):
    try:
        title = request.GET['search']
    except:
        title = ""
    google_apis_link = 'https://www.googleapis.com/books/v1/volumes?q=' + title
    json_dict = requests.get(google_apis_link).json()
    if "liked" in request.session:
        for i in json_dict['items']:
            if i['id'] in request.session['liked']:
                i['enable'] = True
            else:
                i['enable'] = False
    return JsonResponse(json_dict)


@csrf_exempt
def fav(request):
    if request.method == "POST":
        liked = request.session['liked']
        id_book = request.POST['id']
        if id_book not in liked:
            liked.append(id_book)
        request.session['liked'] = liked
        return JsonResponse({'totalfav': len(liked)})
    return JsonResponse({'fail': 'GET Method not allowed'})


@csrf_exempt
def unfav(request):
    if request.method == "POST":
        liked = request.session['liked']
        id_book = request.POST['id']
        if id_book in liked:
            liked.remove(id_book)
        request.session['liked'] = liked
        return JsonResponse({'totalfav': len(liked)})
    return JsonResponse({'fail': 'GET Method not allowed'})