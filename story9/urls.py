from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('google-apis-json-books/', views.google_apis_books, name='google-apis-books'),
    path('fav/', views.fav, name='fav'),
    path('unfav/', views.unfav, name='unfav'),
]
