from django.shortcuts import render
from django.http import HttpResponseRedirect
from .forms import Schedule_Form
from .models import Schedule

response = {}


# Create your views here.
def index(request):
    schedule = Schedule.objects.all()
    response['schedule'] = schedule
    response['title'] = 'My Schedule'

    html = 'jadwalku/index.html'
    return render(request, html, response)


def addschedule(request):
    response['schedule_form'] = Schedule_Form
    response['title'] = 'Add Schedule'

    html = 'jadwalku/add.html'

    return render(request, html, response)


def saveschedule(request):
    form = Schedule_Form(request.POST or None)
    if (request.method == 'POST' and form.is_valid()):
        response['date'] = request.POST['date']
        response['time'] = request.POST['time']
        response['name'] = request.POST['name']
        response['place'] = request.POST['place']
        response['category'] = request.POST['category']
        schedule = Schedule(date=response['date'], time=response['time'], name=response['name'],
                            place=response['place'], category=response['category'])
        schedule.save()
        return HttpResponseRedirect('/schedule/')
    else:
        return HttpResponseRedirect('/schedule/')


def delete_all(request):
    Schedule.objects.all().delete()
    schedule = Schedule.objects.all()
    response['schedule'] = schedule
    response['title'] = 'My Schedule'
    return render(request, 'jadwalku/index.html', response)
