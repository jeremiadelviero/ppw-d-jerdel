from django.db import models

# Create your models here.
class Schedule(models.Model):
    date = models.DateField(null=True)
    time = models.TimeField(null=True)
    name = models.CharField(max_length=200)
    place = models.CharField(max_length=200)
    category = models.CharField(max_length=200)

    def __str__(self):
        return self.name
