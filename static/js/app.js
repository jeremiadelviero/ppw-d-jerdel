$(document).foundation();
$(document).ready(function () {
    $(".loader").fadeOut("slow");
    $(function () {
        $("#accordion").accordion();
    });
    $("#button-theme").click(function () {
        $("html").toggleClass("bg-dark delayA delayB");
        $("body").toggleClass("bg-dark delayA delayB");
        $("#navbar1").toggleClass("navbar-dark delayA delayB");
        $("#navbar2").toggleClass("navbar-dark delayA delayB");
        $("#responsive-menu").toggleClass("navbar-dark delayA delayB");
        $("#responsive-bar").toggleClass("navbar-dark delayA delayB");
        $("#responsive-button").toggleClass("dark white delayA delayB");
        $(".active-page").toggleClass("active-page-dark delayA delayB")
        $(".grid-container").toggleClass("font-white delayA delayB");
    });
});
