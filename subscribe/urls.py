from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('check_all', views.check_all, name='check_all'),
    path('check_email', views.check_email_registered, name='check_email_registered'),
    path('get_all', views.get_all, name='get_all'),
    path('delete', views.unsubscribe, name='delete'),
]
