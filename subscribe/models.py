from django.db import models


# Create your models here.
class Subscriber(models.Model):
    name = models.CharField(max_length=300)
    email = models.EmailField()
    password = models.CharField(max_length=300)

    def __str__(self):
        return self.email
