from django.shortcuts import render

active = 'active-page'


def index(request):
    response = {'index': active, 'title': 'Home'}
    return render(request, 'profilku/index.html', response)


def portfolio(request):
    response = {'portfolio': active, 'title': 'Portfolio'}
    return render(request, 'profilku/portfolio.html', response)


def about(request):
    response = {'about': active, 'title': 'About'}
    return render(request, 'profilku/about.html', response)


def contact(request):
    response = {'contact': active, 'title': 'Contact'}
    return render(request, 'profilku/contact.html', response)

