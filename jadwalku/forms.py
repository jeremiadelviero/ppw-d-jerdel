from django import forms

class Schedule_Form(forms.Form):
    error_messages = {
        'required': 'Please type',
    }
    date_attrs = {
        'type': 'date',
        'placeholder':'Date'
    }
    time_attrs = {
        'type': 'time',
        'placeholder': 'Time'
    }
    name_attrs = {
        'type': 'text',
        'placeholder':'Name'
    }
    place_attrs = {
        'type': 'text',
        'placeholder':'Place'
    }
    category_attrs = {
        'type': 'text',
        'placeholder':'Category'
    }

    date = forms.DateField(label='', required=True, widget=forms.DateInput(attrs=date_attrs))
    time = forms.TimeField(label='', required=True, widget=forms.TimeInput(attrs=time_attrs, format='%H:%M'))
    name = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=name_attrs))
    place = forms.CharField(label='', required=True, max_length=27, widget=forms.TextInput(attrs=place_attrs))
    category = forms.CharField(label='', required=True, widget=forms.TextInput(attrs=category_attrs))
