$(document).ready(function () {
    function csrfSafeMethod(method) {
        // these HTTP methods do not require CSRF protection
        return (/^(GET|HEAD|OPTIONS|TRACE)$/.test(method));
    }

    $.ajaxSetup({
        beforeSend: function (xhr, settings) {
            if (!csrfSafeMethod(settings.type) && !this.crossDomain) {
                xhr.setRequestHeader("X-CSRFToken", csrftoken);
            }
        }
    });

    function delay(callback, ms) {
        var timer = 0;
        return function () {
            var context = this, args = arguments;
            clearTimeout(timer);
            timer = setTimeout(function () {
                callback.apply(context, args);
            }, ms || 0);
        };
    }
    function quilting () {
        $.ajax({
            type: "GET",
            url: google_apis_url + "?search=quilting",
            dataType: 'json',
            context: document.body,
            success: function (data) {
                $("#tbody-books").empty();
                var img = "";
                try {
                    $.each(data["items"], function (i, book) {
                        img +=
                            `
                    <tr>
                        <td><img src="${book["volumeInfo"]["imageLinks"]["smallThumbnail"]}" alt="" width="80"></td>
                        <td>${book["volumeInfo"]["title"]}</td>
                        <td>${book["volumeInfo"]["authors"][0]}</td>
                        <td>${book["volumeInfo"]["publishedDate"]}</td>
                        <td>
                        `
                    if (book['enable']) {
                        img +=
                        `
                            <img id="${book["id"]}" class="favorited" src="${star_gold}" alt=""
                                 width="36">
                        </td>
                    </tr>
                    `
                    } else {
                        img +=
                        `
                            <img id="${book["id"]}" class="notfavorited" src="${star_transparent}" alt=""
                                 width="36">
                        </td>
                    </tr>
                    `
                    }
                    })
                } catch (err) {
                    img = "";
                }
                $("#tbody-books").append(img);
            }
        });
    };
    quilting();

    $("#search").keyup(delay(function (e) {
        var search_query = $('#search').val()
        $.ajax({
            type: "GET",
            url: google_apis_url + "?search=" + search_query,
            dataType: 'json',
            context: document.body,
            success: function (data) {
                $("#tbody-books").empty();
                var img = "";
                try {
                    $.each(data["items"], function (i, book) {
                        img +=
                            `
                    <tr>
                        <td><img src="${book["volumeInfo"]["imageLinks"]["smallThumbnail"]}" alt="" width="80"></td>
                        <td>${book["volumeInfo"]["title"]}</td>
                        <td>${book["volumeInfo"]["authors"][0]}</td>
                        <td>${book["volumeInfo"]["publishedDate"]}</td>
                        <td>
                        `
                    if (book['enable']) {
                        img +=
                        `
                            <img id="${book["id"]}" class="favorited" src="${star_gold}" alt=""
                                 width="36">
                        </td>
                    </tr>
                    `
                    } else {
                        img +=
                        `
                            <img id="${book["id"]}" class="notfavorited" src="${star_transparent}" alt=""
                                 width="36">
                        </td>
                    </tr>
                    `
                    }
                    })
                } catch (err) {
                    img = "";
                }
                $("#tbody-books").append(img);
            }
        });
    }, 500));


    $(document).on('click', ".notfavorited", function () {
        var $this = $(this);
        $.ajax({
            type: "POST",
            url: "/story9/fav/",
            data: {
                'id': this.id,
            },
            dataType: 'json',
            success: function (resp) {
                $this.removeClass("notfavorited");
                $this.addClass("favorited");
                $this.attr('src', star_gold);
                $('.favorites-count').text(resp.totalfav);
            }
        });
    });

    $(document).on('click', ".favorited", function () {
        var $this = $(this);
        $.ajax({
            type: "POST",
            url: "/story9/unfav/",
            data: {
                'id': this.id,
            },
            dataType: 'json',
            success: function (resp) {
                $this.removeClass("favorited");
                $this.addClass("notfavorited");
                $this.attr('src', star_transparent);
                $('.favorites-count').text(resp.totalfav);
            }
        });
    });

    $(function () {
        $('.search')
            .bind('click', function () {
                $(".search-field").toggleClass("expand-search");

                // if the search field is expanded, focus on it
                if ($(".search-field").hasClass("expand-search")) {
                    $(".search-field").focus();
                }
            })
    });
});
