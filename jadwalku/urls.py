from django.urls import path
from . import views

urlpatterns = [
    path('', views.index, name='index'),
    path('add/', views.addschedule, name='addschedule'),
    path('save/', views.saveschedule, name='saveschedule'),
    path('delete_all/', views.delete_all, name='delete_all'),
]
